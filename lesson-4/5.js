function createArray(value, length) { 
    const arr = [];
    if((typeof value === 'number'|| typeof value === 'string'|| typeof value === 'object' || Array.isArray(value) === true) && typeof length === 'number'){
        for (let i = 0; i < length; i++) {
            arr.push(value);
        }
        return arr;
    } else {
        throw new Error('first argumant must be number or string or object or array, second argument must be number')
    }	
}

const result = createArray('x', 5);

console.log(result); // [ x, x, x, x, x ]

// exports.createArray = createArray;




