let reduce = function(arr, callback, startValue = 0) {
    let result = startValue;
    if (Array.isArray(arr) === true && typeof callback === 'function'){
      for (let i = 0; i < arr.length; i++) {
        result = callback.call(null, result, arr[i], i, arr);
      }
      return result;
    } else {
      throw new Error('First argument must be an array, second agument must be a function!')
    }
  };

const array = [1, 2, 3, 4, 5];
const INITIAL_ACCUMULATOR = 6;


const result = reduce(
    array,
    (accumulator, element, index, arrayRef) => {
        console.log(`${index}:`, accumulator, element, arrayRef);

        return accumulator + element;
    },
    INITIAL_ACCUMULATOR,
);

console.log(result)