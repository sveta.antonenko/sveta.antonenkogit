const collect = (arr) => {
    if (Array.isArray(arr)) {
      const lineArray = arr.flat(Infinity) 
      if (!lineArray.length) {
        return 0
      } 
  
      return lineArray.reduce(
        (res, item) => {
          if (typeof item === "number") {
            return res + item;
          }
          
          throw new Error('array should only contain numbers')
        }, 0);
    }
  
    throw new Error('Argument must be an array!')
}


const array1 = [[[1, 2], [1, 2]], [[2, 1], [1, 2]]];
console.log(collect(array1)); // 12

const array2 = [[[[[1, 2]]]]];
console.log(collect(array2)); // 3

const array3 = [[[[[1, 2]]], 2], 1];
console.log(collect(array3)); // 6

const array4 = [[[[[]]]]];
console.log(collect(array4)); // 0

const array5 = [[[[[], 3]]]];
console.log(collect(array5)); // 3

// exports.collect = collect;