const array = [
    false,
    'Привет.',
    2,
    'Здравствуй.',
    [],
    'Прощай.',
    {
        name: 'Уолтер',
        surname: 'Уайт',
    },
    'Приветствую.',
];

function inspect(array){
 if(Array.isArray(array) === true){
    const newArray = array.filter(element => typeof element === 'string')
    return newArray.map(function(x){
        return x.length
    })
 } else {
     throw new Error('Argument must be an array!')
 }
}

console.log(inspect(array))




