function checkSpam(source, spam){
    if(typeof source === 'string' && typeof spam === 'string'){
        let searchSpam = new RegExp(spam,'i');
        let result = source.search(searchSpam);
        return (result > 0) ? true : false;
    }
}

console.log(checkSpam('pitterXXX@gmail.com','xxx'));
console.log(checkSpam('pitterxxx@gmail.com','XXX'));
console.log(checkSpam('pitterXXX@gmail.com','sss'));