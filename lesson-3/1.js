function upperCaseFirst(str){
    if(typeof str === 'string'){
        return str.charAt(0).toUpperCase() + str.slice(1)
    }
}

console.log(upperCaseFirst('pitter'))
console.log(upperCaseFirst(''))