const PRICE = '$120';

function extraCurrencyValue(sourse){
    if( typeof sourse === 'string'){
        return +sourse.slice(1)
    } else {
        return null
    }
}

console.log(extraCurrencyValue(PRICE));
console.log(typeof extraCurrencyValue(PRICE));
console.log(extraCurrencyValue({}));
