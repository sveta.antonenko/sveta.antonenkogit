function truncate(string, maxLength){
    if (typeof string === 'string' && typeof maxLength === 'number'){
        if (string.length > maxLength){
            return string.slice(0, maxLength-3) + '...';
        } else {
            return string;
        }
    }

}

console.log(truncate('Вот, что мне хотелось бы сказать на эту тему:', 21));