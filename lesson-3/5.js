const person = {
    rate: 3,
    hours: 3,
    get salary(){
        if(typeof this.rate === 'number' && typeof this.hours === 'number'){
            return 'Зарплата за проект составляет ' + this.rate*this.hours + '$'
        } else {
            return 'Свойства rate и hours должны содержать число'
        }
    }
}

Object.defineProperties(person, {
    rate: {
        writable: true,
        configurable: false
    },
    hours: {
        writable: true,
        configurable: false
    }
})

console.log(person.salary)