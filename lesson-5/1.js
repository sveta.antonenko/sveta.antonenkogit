function f (n){
    if(typeof n === 'number'){
        return n ** 3
    } else{
        throw new Error('argument must be a number')
    }
};

console.log(f(2))