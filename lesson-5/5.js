function createFibonacciGenerator(){
    let count = 1 
    return () => {
        function fib(count) {
            return count <= 1 ? count : fib(count - 1) + fib(count - 2);
        }            
        return {
            print: () => fib(count++), 
            reset: function(){count = 1} 
        } 
    }
}

const generator1 = createFibonacciGenerator();
 
console.log(generator1().print()); // 1
console.log(generator1().print()); // 1
console.log(generator1().print()); // 2
console.log(generator1().print()); // 3
console.log(generator1().reset()); // undefined
console.log(generator1().print()); // 1
console.log(generator1().print()); // 1
console.log(generator1().print()); // 2

const generator2 = createFibonacciGenerator();

console.log(generator2().print()); // 1
console.log(generator2().print()); // 1
console.log(generator2().print()); // 2

