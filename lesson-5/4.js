function getDivisors(n) {
    const arr = [];
    if(n >= 1 || typeof n === 'number' ){
        for (let i = 1; i <= n; i++) {
            if (n % i == 0) {
               arr.push(i);
            }
        }
        return arr
    } else {
        throw new Error('argument must be a number >= 1');
    }
}

console.log(getDivisors('12'));