function f(){
    let result = 0;
    for(let i = 0; i < arguments.length; i++){
        if(typeof arguments[i] === 'number'){
           result += arguments[i];
        } else{
            throw new Error('Arguments must be numbers')
        }
    }
    return result
};